"use-strict";
// body
const container=document.querySelector(".container");
const seats=document.querySelectorAll(".row .seat:not(.sold)");

// hasil
const count=document.getElementById("count");
const total=document.getElementById("total");
const BioskopSelect=document.getElementById("Bioskop");


// Select button
const pesan=document.querySelector(".pesan-btn");
const reset=document.querySelector(".reset-btn");

populateUI();

let ticketPrice=+BioskopSelect.value;

// Save selected Bioskop index dan harga
function setBioskopData(BioskopIndex, BioskopPrice) 
{
  localStorage.setItem("selectedBioskopIndex", BioskopIndex);
  localStorage.setItem("selectedBioskopPrice", BioskopPrice);
}

// Update total dan jumlah harga kursi
function updateSelectedCount() 
{
  const selectedSeats=document.querySelectorAll(".row .seat.selected");
  const seatsIndex=[...selectedSeats].map((seat) => [...seats].indexOf(seat));

  localStorage.setItem("selectedSeats", JSON.stringify(seatsIndex));
  const selectedSeatsCount=selectedSeats.length;

  count.innerText=selectedSeatsCount;
  total.innerText=selectedSeatsCount * ticketPrice + ( 1000 * selectedSeatsCount);

  setBioskopData(BioskopSelect.selectedIndex, BioskopSelect.value);
}

// Bioskop event select
BioskopSelect.addEventListener("change", (e) => 
{
  ticketPrice = +e.target.value;
  setBioskopData(e.target.selectedIndex, e.target.value);
  updateSelectedCount();
});

// mengambil data dari localstorage dan populate UI
function populateUI() 
{
  const selectedSeats=JSON.parse(localStorage.getItem("selectedSeats"));

  if (selectedSeats !== null && selectedSeats.length > 0) 
  {
    seats.forEach((seat, index) => {
      if (selectedSeats.indexOf(index) > -1) 
      {
        seat.classList.add("selected");
      }
    });
  }

  const selectedBioskopIndex=localStorage.getItem("selectedBioskopIndex");
  if (selectedBioskopIndex !== null) 
  {
    BioskopSelect.selectedIndex = selectedBioskopIndex;
  }
}


// Seat event click
container.addEventListener("click", (e) => 
{
  if (e.target.classList.contains("seat") && !e.target.classList.contains("sold")) 
  {
    e.target.classList.toggle("selected");
    updateSelectedCount();
  } else if (e.target.classList.contains("sold")) 
  {
    const seatName=e.target.getAttribute("id") + e.target.textContent;
    alert("Seat " + seatName + " sudah terisi. Silakan pilih seat lainnya");
  }
});

pesan.addEventListener("click", (e) => 
{
  // mengambil selected seat
  const selectedSeat=document.querySelectorAll(".row .seat.selected");
  for (let i = 0; i < selectedSeat.length; i++) {
    selectedSeat[i].classList.add("sold");
  }
});

reset.addEventListener("click", (e) => 
{
  const selectedSeat=document.querySelectorAll(".row .seat.selected");
  for (let i = 0; i < selectedSeat.length; i++) 
  {
    selectedSeat[i].classList.remove("selected");
    selectedSeat[i].classList.remove("sold");
  }
  count.innerText=0;
  total.innerText=0;
});
//count and total set
updateSelectedCount();
